# README

Este repositorio contiene mi configuracion de `.bashrc` y mis alias

## Instrucciones recomendadas

- `$ git clone https://gitlab.com/solimanlab/bashrc.git`

- `$ cd bashrc`

- `$ ./ejecutar`

- `$ source ~/.bashrc` (opcional)

## Notas:

Para ver los alias disponibles y para crear nuevos alias, ver y editar el archivo `.aliasrc`
